# HiSpark-Pegasus 智能小车样例

## 一、智能小车套件外设控制样例

### 1.1 文件说明

| 文件             | 说明           |
| ---------------- | -------------- |
| robot_hcsr04.c   | 超声波测距样例 |
| robot_l9110s.c   | 电机驱动样例   |
| robot_sg90.c     | 舵机控制样例   |
| robot_sg90_mid.c | 舵机校准       |
| robot_tcrt5000.c | 红外传感器样例 |

### 1.2 使用方法

1、直接将robot文件夹拷贝到OpenHarmony源码applications/sample/wifi-iot/app目录下。

2、需要编译哪个样例，就将对应的文件的注释打开，将其他的文件注释掉

​      比如需要编译超声波样例，BUILD.gn文件修改为如下内容：

```
static_library("robot_demo") {
    sources = [
        "robot_hcsr04.c",
        #"robot_l9110s.c",
        #"robot_sg90_mid.c",
        #"robot_sg90.c",
        #"robot_tcrt5000.c"
        #"l9110s_pwm_control.c"
    ]

    include_dirs = [
        "//utils/native/lite/include",
        "//kernel/liteos_m/kal/cmsis",
        "//base/iot_hardware/peripheral/interfaces/kits",
    ]
}
```

3、修改app目录下的BUILD.gn文件

```
import("//build/lite/config/component/lite_component.gni")

lite_component("app") {
    features = [
        "robot:robot_demo"
    ]
}
```

## 二、智能循迹避障小车样例代码

### 2.1 文件说明

| 文件            | 说明                   |
| --------------- | ---------------------- |
| ssd1306         | ssd1306驱动代码        |
| robot_control.c | 智能小车主要控制代码   |
| robot_hcsr04.c  | 智能小车超声波测距代码 |
| robot_l9110s.c  | 智能小车电机控制代码   |
| robot_sg90.c    | 智能小车舵机控制代码   |
| ssd1306_test.c  | 智能小车OLED显示屏代码 |
| trace_model.c   | 智能小车循迹功能代码   |

### 2.2 使用方法

1、直接将robot_demo文件夹拷贝到OpenHarmony源码applications/sample/wifi-iot/app目录下。

2、需要编译哪个样例，就将对应的文件的注释打开，将其他的文件注释掉

​      比如需要编译超声波样例，BUILD.gn文件修改为如下内容：

```
static_library("robot_demo") {
    sources = [
        "robot_hcsr04.c",
        "robot_l9110s.c",
        "robot_sg90.c",
        "trace_model.c",
        "ssd1306_test.c",
        "robot_control.c"
    ]

    include_dirs = [
        "./ssd1306",
        "//utils/native/lite/include",
        "//kernel/liteos_m/kal/cmsis",
        "//base/iot_hardware/peripheral/interfaces/kits",
    ]
}
```

3、修改app目录下的BUILD.gn文件

```
import("//build/lite/config/component/lite_component.gni")

lite_component("app") {
    features = [
        "robot_demo:robot_demo"
    ]
}
```


## 三、DAYU200遥控HiSpark-Pegasus智能小车样例
本样例通过TCP socket通信实现DAYU200开发板(OpenHarmony标准系统) 遥控 HiSpark-Pegasus智能小车(OpenHarmony轻量系统)前进、后退、左转、右转、停止等操作，实现了一个简单的OpenHarmony南北向互通案例。

### 3.1 目录及文件说明

```
   DAYU200遥控HiSpark-Pegasus智能小车样例
    ├── DAYU200(3568)                    # DAYU200端hap应用(基于DevEco Studio 3.1.0.500、sdk 3.2.12.2)
    │    ├── entry-default-signed.hap    # DAYU200端hap应用安装包
    │    └── car_socket_control          # DAYU200端hap应用源码
    ├── HiSpark-Pegasus-smart-car(3861)  
    │    ├── Hi3861_wifiiot_app_allinone.bin # HiSpark-Pegasus智能小车源码编译好的固件(热点名称jjh,密码jjhjjhjjh,TCP端口号5678)
    │    └── car_tcp_control                 # HiSpark-Pegasus智能小车源码
    └── README.md
```

- HiSpark-Pegasus-smart-car(3861)/car_tcp_control文件说明

| 文件名             | 说明                                                        |
| ------------------ | ----------------------------------------------------------- |
| BUILD.gn           | OpenHarmony构建脚本                                          |
| demo_entry_cmsis.c | OpenHarmony liteos-m程序入口                               |
| net_common.h       | 系统网络接口头文件                                          |
| net_demo.h         | demo脚手架头文件                                            |
| net_params.h       | 网络参数，包括WiFi热点信息、端口信息                    |
| car_tcp_server_test.c | TCP socket控制小车实现代码                         |
| wifi_connecter.c   | OpenHarmony WiFi STA模式API的封装实现文件 |
| wifi_connecter.h   | OpenHarmony WiFi STA模式API的封装头文件   |

### 3.2 使用方法

### 3.2.1 3861侧

1、直接将HiSpark-Pegasus-smart-car(3861)下的car_tcp_control文件夹拷贝到OpenHarmony源码applications/sample/wifi-iot/app目录下。


2、修改`net_params.h`文件的相关代码：
PARAM_HOTSPOT_SSID 设置为要连接的热点名称、
PARAM_HOTSPOT_PSK  设置为要连接的热点密码、
PARAM_SERVER_PORT 设置 3861开发板（作为TCP服务端） TCP socket端口号。

3、将car_tcp_control加入到OpenHarmony源码的编译

```
import("//build/lite/config/component/lite_component.gni")

lite_component("app") {
    features = [
        "car_tcp_control:car_tcp_control_demo"
    ]
}
```

### 3.2.2 3568侧

DAYU200端hap应用基于OpenHarmony3.2release DevEco Studio 3.1.0.500、sdk 3.2.12.2环境开发。

1、DAYU200(3568)目录下提供了编译好了的安装包entry-default-signed.hap,安装好应用。

2、在应用中填写3861开发板的ip(串口打印出来的)和3861侧`net_params.h`中填写的TCP socket端口号到3568端，以连接3861开发板。

![](DAYU200遥控HiSpark-Pegasus智能小车样例/media/DAYU200%E7%AB%AFhap%E5%BA%94%E7%94%A8.jpeg)