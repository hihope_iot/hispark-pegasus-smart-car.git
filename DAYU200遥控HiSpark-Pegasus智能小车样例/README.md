# DAYU200遥控HiSpark-Pegasus智能小车样例
本样例通过TCP socket通信实现DAYU200开发板(OpenHarmony标准系统) 遥控 HiSpark-Pegasus智能小车(OpenHarmony轻量系统)前进、后退、左转、右转、停止等操作，实现了一个简单的OpenHarmony南北向互通案例。

## 目录及文件说明

```
   DAYU200遥控HiSpark-Pegasus智能小车样例
    ├── DAYU200(3568)                    # DAYU200端hap应用(基于DevEco Studio 3.1.0.500、sdk 3.2.12.2)
    │    ├── entry-default-signed.hap    # DAYU200端hap应用安装包
    │    └── car_socket_control          # DAYU200端hap应用源码
    ├── HiSpark-Pegasus-smart-car(3861)  
    │    ├── Hi3861_wifiiot_app_allinone.bin # HiSpark-Pegasus智能小车源码编译好的固件(热点名称jjh,密码jjhjjhjjh,TCP端口号5678)
    │    └── car_tcp_control                 # HiSpark-Pegasus智能小车源码
    └── README.md
```

- HiSpark-Pegasus-smart-car(3861)/car_tcp_control文件说明

| 文件名             | 说明                                                        |
| ------------------ | ----------------------------------------------------------- |
| BUILD.gn           | OpenHarmony构建脚本                                          |
| demo_entry_cmsis.c | OpenHarmony liteos-m程序入口                               |
| net_common.h       | 系统网络接口头文件                                          |
| net_demo.h         | demo脚手架头文件                                            |
| net_params.h       | 网络参数，包括WiFi热点信息、端口信息                    |
| car_tcp_server_test.c | TCP socket控制小车实现代码                         |
| wifi_connecter.c   | OpenHarmony WiFi STA模式API的封装实现文件 |
| wifi_connecter.h   | OpenHarmony WiFi STA模式API的封装头文件   |

## 使用方法

### 3861侧

1、直接将HiSpark-Pegasus-smart-car(3861)下的car_tcp_control文件夹拷贝到OpenHarmony源码applications/sample/wifi-iot/app目录下。


2、修改`net_params.h`文件的相关代码：
PARAM_HOTSPOT_SSID 设置为要连接的热点名称
PARAM_HOTSPOT_PSK  设置为要连接的热点密码
PARAM_SERVER_PORT 设置 3861开发板（作为TCP服务端） TCP socket端口号

3、将car_tcp_control加入到OpenHarmony源码的编译

```
import("//build/lite/config/component/lite_component.gni")

lite_component("app") {
    features = [
        "car_tcp_control:car_tcp_control_demo"
    ]
}
```

### 3568侧

DAYU200端hap应用基于OpenHarmony3.2release DevEco Studio 3.1.0.500、sdk 3.2.12.2环境开发。

1.DAYU200(3568)目录下提供了编译好了的安装包entry-default-signed.hap,安装好应用后

2、在应用中填写3861开发板的ip(串口打印出来的)和3861侧`net_params.h`中填写的TCP socket端口号到3568端，以连接3861开发板。
![](media/DAYU200%E7%AB%AFhap%E5%BA%94%E7%94%A8.jpeg)

