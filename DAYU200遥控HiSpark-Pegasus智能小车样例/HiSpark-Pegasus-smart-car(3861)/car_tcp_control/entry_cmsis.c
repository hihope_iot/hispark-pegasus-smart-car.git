#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"

#include "net_demo.h"
#include "net_params.h"
#include "wifi_connecter.h"

#define STACK_SIZE         (10240)
#define DELAY_TICKS_10     (10)

static void TCP_keyboard(void)
{
    //wifi连接的部分
    WifiDeviceConfig config = {0};
    strcpy_s(config.ssid, WIFI_MAX_SSID_LEN, PARAM_HOTSPOT_SSID);
    strcpy_s(config.preSharedKey, WIFI_MAX_KEY_LEN, PARAM_HOTSPOT_PSK);
    config.securityType = PARAM_HOTSPOT_TYPE;
    osDelay(DELAY_TICKS_10);
    int netId = ConnectToHotspot(&config);

    //wifi tcp连接的部分
    NetDemoTest(PARAM_SERVER_PORT, PARAM_SERVER_ADDR);

}

static void TCP_keyboard_Demo(void)
{
    osThreadAttr_t attr;

    attr.name = "TCP_keyboard";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = STACK_SIZE;
    attr.priority = osPriorityNormal;

    if (osThreadNew(TCP_keyboard, NULL, &attr) == NULL) {
        printf("[TCP_keyboard_Demo] Falied to create TCP_keyboard!\n");
    }
}

SYS_RUN(TCP_keyboard_Demo);